watcher-dashboard (12.0.0-3) unstable; urgency=medium

  * Really close bug (Closes: #1090706).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 08:40:20 +0100

watcher-dashboard (12.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closse: #1090706).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 14:02:36 +0100

watcher-dashboard (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 17:11:04 +0200

watcher-dashboard (12.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 00:32:44 +0200

watcher-dashboard (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 10:31:27 +0200

watcher-dashboard (11.0.0-3) unstable; urgency=medium

  * Removed build-depends on python3-django-nose (Closes: #1070287).

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 May 2024 08:59:36 +0200

watcher-dashboard (11.0.0-2) unstable; urgency=medium

  * Removed python3-nose-exclude from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 19 Apr 2024 10:30:25 +0200

watcher-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2024 08:06:37 +0200

watcher-dashboard (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed applied upstream patches:
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 18:31:55 +0100

watcher-dashboard (10.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 12:30:48 +0200

watcher-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans even better.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 14:28:17 +0200

watcher-dashboard (9.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1045653).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 15:55:39 +0200

watcher-dashboard (9.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 17:26:45 +0200

watcher-dashboard (9.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 16:03:06 +0100

watcher-dashboard (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed py3.11-getfullargspec.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Mar 2023 11:06:57 +0100

watcher-dashboard (8.0.0-2) unstable; urgency=medium

  * Add py3.11-getfullargspec.patch (Closes: #1025195).

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Dec 2022 10:26:46 +0100

watcher-dashboard (8.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 10:47:24 +0200

watcher-dashboard (8.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:03:25 +0200

watcher-dashboard (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 09:19:24 +0200

watcher-dashboard (7.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Add django 4 compat patches (Closes: #1015100):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 Jul 2022 17:56:38 +0200

watcher-dashboard (7.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 09:40:10 +0200

watcher-dashboard (7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 17:12:03 +0100

watcher-dashboard (6.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 22:05:04 +0200

watcher-dashboard (6.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:58:37 +0200

watcher-dashboard (6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (build-)depends on horizon version >= 3:20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Sep 2021 10:14:16 +0200

watcher-dashboard (5.0.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:38:15 +0200

watcher-dashboard (5.0.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 12:50:19 +0200

watcher-dashboard (5.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:44:58 +0200

watcher-dashboard (5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 17:36:32 +0200

watcher-dashboard (5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when statisfied in Bullseye.
  * debhelper-compat 11.
  * Standards-Version: 4.5.1.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 21:35:03 +0100

watcher-dashboard (4.0.0-2) unstable; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Jul 2021 12:23:31 +0200

watcher-dashboard (4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 17:39:13 +0200

watcher-dashboard (4.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-mock from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Sep 2020 09:10:23 +0200

watcher-dashboard (3.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 19:17:12 +0200

watcher-dashboard (3.0.0~rc1-2) unstable; urgency=medium

  * Move the package to the horizon-plugins subgroup on Salsa.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:32:32 +0200

watcher-dashboard (3.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed fix-test-django22.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2020 12:17:22 +0200

watcher-dashboard (2.0.0-3) unstable; urgency=medium

  * Rebuild source only.

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 05 Mar 2020 16:52:48 +0100

watcher-dashboard (2.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Run wrap-and-sort -bastk.

  [ Michal Arbet ]
  * Edit d/copyright

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 23 Oct 2019 10:18:42 +0200

watcher-dashboard (2.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #942445).

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 16 Oct 2019 16:25:24 +0200
